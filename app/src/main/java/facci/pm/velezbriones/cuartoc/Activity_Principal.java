package facci.pm.velezbriones.cuartoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Activity_Principal extends AppCompatActivity implements View.OnClickListener{

    private Button login, registro, buscar, pasarParametro, PRACTICA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__principal);
        setTitle(getString(R.string.practica2));
        login = (Button)findViewById(R.id.BTNLogin);
        registro = (Button)findViewById(R.id.BTNRegistrar);
        buscar = (Button)findViewById(R.id.BTNBuscar);
        pasarParametro = (Button)findViewById(R.id.BTNParametro);
        PRACTICA = (Button)findViewById(R.id.BTNPractica4);

        login.setOnClickListener(this);
        registro.setOnClickListener(this);
        buscar.setOnClickListener(this);
        pasarParametro.setOnClickListener(this);
        PRACTICA.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(Activity_Principal.this, Activity_Login.class);
                startActivity(intent);
                break;

            case R.id.opcionRegistrar:
                intent = new Intent(Activity_Principal.this, Activity_Registrar.class);
                startActivity(intent);
                break;

            case R.id.opcionParametro:
                intent = new Intent(Activity_Principal.this, Activity_Pasar_Parametro.class);
                startActivity(intent);
                break;

            case R.id.opcionBuscar:
                intent = new Intent(Activity_Principal.this, ActivityBuscar.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.BTNBuscar:
                startActivity(new Intent(this, ActivityBuscar.class));
                break;

            case R.id.BTNLogin:
                startActivity(new Intent(this, Activity_Login.class));
                break;

            case R.id.BTNRegistrar:
                startActivity(new Intent(this, Activity_Registrar.class));
                break;

            case R.id.BTNParametro:
                startActivity(new Intent(this, Activity_Pasar_Parametro.class));
                break;

            case R.id.BTNPractica4:
                startActivity(new Intent(this, ActivityTableLayout.class));
                break;
        }

    }
}

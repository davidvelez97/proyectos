package facci.pm.velezbriones.cuartoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Activity_Pasar_Parametro extends AppCompatActivity {

    private Button enviar;
    private EditText dato;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__pasar__parametro);
        setTitle(getString(R.string.enviar));
        enviar = (Button)findViewById(R.id.BTNEnviar);
        dato = (EditText)findViewById(R.id.TXTEnviar);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dato.getText().toString().isEmpty()){
                    dato.setError(getString(R.string.campo));
                }else {
                    Intent intent = new Intent(Activity_Pasar_Parametro.this, Activity_Recibir_Parametro.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("dato", dato.getText().toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    dato.setText("");
                }
            }
        });



    }
}

package facci.pm.velezbriones.cuartoc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Activity_Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__login);
        setTitle(getString(R.string.login));
    }
}

package facci.pm.velezbriones.cuartoc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Activity_Recibir_Parametro extends AppCompatActivity {

    private TextView Dato;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__recibir__parametro);
        setTitle(getString(R.string.recibir));

        Dato = (TextView)findViewById(R.id.LBLRecibir);
        Bundle bundle = this.getIntent().getExtras();
        Dato.setText(bundle.getString("dato"));
    }
}

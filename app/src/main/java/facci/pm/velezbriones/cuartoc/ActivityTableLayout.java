package facci.pm.velezbriones.cuartoc;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityTableLayout extends AppCompatActivity implements View.OnClickListener, FrgUno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener{

    private Button botonFragUno, botonFragDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_layout);
        setTitle(getString(R.string.activity));

        botonFragDos = (Button) findViewById(R.id.btnFrgDos);
        botonFragUno = (Button) findViewById(R.id.btnFrgUno);

        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionLogin2:
                final Dialog dialogLogin = new Dialog(ActivityTableLayout.this);
                dialogLogin.setContentView(R.layout.dlg_login);

                Button button = (Button)dialogLogin.findViewById(R.id.btnAutenticar);
                final EditText Usuario = (EditText)dialogLogin.findViewById(R.id.txtUser);
                final EditText Clave = (EditText)dialogLogin.findViewById(R.id.txtPassword);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Usuario.getText().toString().isEmpty()|| Clave.getText().toString().isEmpty()){
                            Toast.makeText(ActivityTableLayout.this, "Escriba Su Usuario Y Clave", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ActivityTableLayout.this, Usuario.getText().toString()+" "+ Clave.getText().toString(), Toast.LENGTH_SHORT).show();
                            dialogLogin.hide();
                        }

                    }
                });

                dialogLogin.show();

                break;

            case R.id.opcionRegistrar2:

                final Dialog dialogRegistro = new Dialog(ActivityTableLayout.this);
                dialogRegistro.setContentView(R.layout.dlg_registro);

                Button registrar = (Button)dialogRegistro.findViewById(R.id.BTNRegistrar2);
                final EditText usuario = (EditText)dialogRegistro.findViewById(R.id.txtUser2);
                final EditText cedula = (EditText)dialogRegistro.findViewById(R.id.txtCedula);
                final EditText correo = (EditText)dialogRegistro.findViewById(R.id.txtCorreo);
                final EditText clave = (EditText)dialogRegistro.findViewById(R.id.txtPassword2);

                registrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (usuario.getText().toString().isEmpty()|| clave.getText().toString().isEmpty()
                                || cedula.getText().toString().isEmpty()|| correo.getText().toString().isEmpty()){
                            Toast.makeText(ActivityTableLayout.this, "Escriba Sus Datos", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ActivityTableLayout.this, usuario.getText().toString(), Toast.LENGTH_SHORT).show();
                            dialogRegistro.hide();
                        }

                    }
                });

                dialogRegistro.show();
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dos, menu);
        return true;

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FrgUno frgUno = new FrgUno();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor, frgUno);
                transaction.commit();
                break;

            case R.id.btnFrgDos:
                FrgDos frgDos = new FrgDos();
                FragmentTransaction transactionD = getSupportFragmentManager().beginTransaction();
                transactionD.replace(R.id.contenedor, frgDos);
                transactionD.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
